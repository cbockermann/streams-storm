# streams-storm

This package uses [streams framework](https://sfb876.de/streams/) as an abstraction layer upon [Apache Storm](http://storm.apache.org/).
``streams`` simplifies job definition that is translated automatically into a native Storm's topology.

The XML definition of ``streams`` process has not been changed.

* ``stream`` -> spout
* ``process``-> bolt

With the ``copies`` attribute inside the ``<process ...>`` tag the user controls the level of parallelism.
Setting it higher results in multiple bolt instances processing the incoming data stream in parallel.
More details about ``services`` and ``queues`` can be found in the documentation about ``streams`` framework.

Apache Storm can [guarantee at-least-once message delivery](http://storm.apache.org/releases/current/Guaranteeing-message-processing.html).
Acknowledging each message might have some impact on the throughput of the system. 
``streams-storm`` allows for turning this acknowledgement mechanism on and off by adding ``ack='true'`` (default: false) to the first tag of the xml (container/application).


## Packaging

```bash
# package for deployment
mvn -P deploy package

# package for local start and transformation step
mvn -P standalone package
```

As a result following ``jar`` files are produced:

```bash
# run locally
streams-storm-{version}-storm-compiled.jar 

# does not contain storm, will be deployed
streams-storm-{version}-storm-provided.jar 
```

Afterwards all you need to do is 

```
java -jar -Dnimbus.seeds=localhost -Dstorm.jar=target/streams-storm-{version}-storm-provided.jar target/streams-storm-{version}-storm-compiled.jar streams_process.xml
```

``-Dnimbus.seeds`` is used to define the IP adress of the storm nimbus and ``-Dstorm.jar`` declares the path to the jar package that will be deployed to the storm cluster.
Jar file containing storm is used to run Storm Topology Builder on your machine and run the transformation and deployment process.

## Using script

As a shortcut there is a script ``packageScriptForDeploy.sh`` in the top level of this repository. It accepts as argument 

* ``deployRun``: package everything and deploy to local storm installation with a provided xml file
* ``run``: don't package (use existing packages) and just deploy those packages with a provided xml file
* With no parameter given this script just packages everything and doesn't deploy anything.

# Start storm locally

To run storm you need Java 6 and Python 2.6.6. 

* Download [storm](https://storm.apache.org/downloads.html)
* Download [zookeeper](http://zookeeper.apache.org/releases.html)
* Start zookeeper (``bin/zkServer.sh start``), [more details](http://zookeeper.apache.org/doc/r3.3.3/zookeeperStarted.html#sc_InstallingSingleMode)
* From the unzipped storm archive run: 
  * ``bin/storm nimbus`` to start nimbus (master node)
  * ``bin/storm ui`` to start the graphical interface (``localhost:8080``)
  * ``bin/storm supervisor`` to start a supervisor with 4 worker nodes (threads)

For more tuning possibilities you will find ``conf/storm.yaml`` with properties.

**IMPORTANT**: The above setting only works for one supervisor on the local machine. 
You can not start two supervisors this way.
The easieast solution is to copy the whole storm folder and start a supervisor with differnt ports by changing those properties in ``conf/storm.yaml``.

**MORE DETAILS** can be found on the [documentation pages of Apache Storm](https://storm.apache.org/documentation/Setting-up-a-Storm-cluster.html).