/*
 *  streams library
 *
 *  Copyright (C) 2011-2014 by Christian Bockermann, Hendrik Blom
 * 
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.ByteArrayInputStream;
import java.net.InetAddress;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import stream.Constants;
import stream.Data;
import stream.ProcessContext;
import stream.Processor;
import stream.ProcessorList;
import stream.StormRunner;
import stream.StreamTopology;
import stream.Subscription;
import stream.runtime.ApplicationContext;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessorFactory;
import stream.storm.config.Grouping;
import stream.util.Variables;
import streams.storm.TaskContext;
import streams.storm.TupleWrapper;

/**
 * <p> This bolt implementation wraps a process configuration and creates the process including its
 * inner element to provide a bolt that behaves the same as a regular streams process. </p> <p>
 * TODO: Currently, services are not injected into bolts as there is no service facade
 * implementation for storm, yet. </p>
 *
 * @author Christian Bockermann, Alexey Egorov
 */
public class ProcessBolt extends AbstractBolt {

    /**
     * The unique class ID
     */
    private static final long serialVersionUID = -924312414467186051L;

    static Logger log = LoggerFactory.getLogger(ProcessBolt.class);

    /**
     * List of processors contained in this bolt.
     */
    transient ProcessorList process;

    /**
     * System and custom process variables
     */
    protected final Variables variables;

    /**
     * List of output bolts.
     */
    protected String[] outputs;

    /**
     * Bolt context containing such information as process and application names (id).
     */
    BoltContext ctx;

    /**
     * List of services
     */
    private List<StormService> stormServices;

    /**
     * The list of subscribers (e.g. output queues,...) that need to be connected to this bolt
     */
    final Set<Subscription> subscriptions = new LinkedHashSet<>();

    /**
     * ID of this component in the Storm context
     */
    String componentId;

    /**
     * ID of the task in the Storm context
     */
    String taskId;

    /**
     * Save the current tuple that is used by the QueueWrapper to correctly emit the tuple.
     */
    Tuple currentTuple = null;

    /**
     * Key used for grouping the data stream
     */
    String key = null;

    /**
     * Option to enable acknowledging of messages
     */
    boolean acking = false;

    /**
     * The bolt implementation requires an XML configuration (the complete container XML as string)
     * and the ID that identifies the corresponding process within that XML.
     */
    public ProcessBolt(ApplicationContext ctx, String xmlConfig, String uuid, StreamTopology streamTopology)
            throws Exception {
        super(ctx, xmlConfig, uuid);
        this.variables = new Variables(streamTopology.variables);
        this.stormServices = streamTopology.stormServices;
        this.acking = streamTopology.acking;

        this.ctx = new BoltContext(ctx, uuid);
        this.ctx.set(Constants.APPID, variables.get(Constants.APPID));

        // first step: instantiate the process and all of its
        // processors - this is required to determine any references to
        // sinks/services
        //
        log.debug("Creating process bolt '{}'", uuid);
        variables.set("copy.id", String.valueOf(UUID.randomUUID()));
        createProcess();
    }

    /**
     * Retrieve currently processed tuple for QueueWrapper for correct emission
     */
    public Tuple getCurrentTuple() {
        return currentTuple;
    }

    @Override
    public void declareOutputFields(final OutputFieldsDeclarer outputFieldsDeclarer) {

        super.declareOutputFields(outputFieldsDeclarer);

        for (Subscription sub : getSubscriptions()) {
            // if a key has been provided, use it as an additional field
            // this is used for grouping by a subscriber
            Fields fields;
            if (key != null) {
                log.info("Declaring output stream '{}' with keys {} and {}",
                        sub.subscriber(), key, TupleWrapper.DATA_KEY);
                fields = new Fields(key, TupleWrapper.DATA_KEY);
            } else {
                log.info("Declaring output stream '{}'", sub.subscriber());
                fields = new Fields(TupleWrapper.DATA_KEY);
            }
            outputFieldsDeclarer.declareStream(sub.subscriber(), fields);
        }
    }

    /**
     * This method returns the list (set) of queues ('sinks') that are referenced by any of the
     * processors of this process bolt. These need to artificially be connected to this bolt
     * (subscription model).
     *
     * @return set of subscriptions
     */
    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    /**
     * This method creates the inner processors of this process bolt.
     *
     * @return list of processors inside a process
     */
    protected ProcessorList createProcess() throws Exception {

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document config = builder.parse(new ByteArrayInputStream(xmlConfig.getBytes()));

        Element element = StormRunner.findElementByUUID(config.getDocumentElement(), uuid);

        if (element == null) {
            log.error("Failed to find process for uuid '{}' in the XML!", uuid);
            throw new Exception("Failed to find process for uuid '" + uuid + "' in the XML!");
        }

        if (element.hasAttribute("key")) {
            key = element.getAttribute("key");
        }

        ctx.set("process", uuid + "@" + InetAddress.getLocalHost().getCanonicalHostName()
                + "-" + UUID.randomUUID());

        ObjectFactory obf = ObjectFactory.newInstance();
        obf.addVariables(this.variables);
        ProcessorFactory pf = new ProcessorFactory(obf);

        // The handler injects wrappers for any QueueService accesses, thus
        // effectively doing the queue-flow injection
        //
        QueueInjection queueInjection = new QueueInjection(uuid, output);
        queueInjection.setProcessBolt(this);
        pf.addCreationHandler(queueInjection);

        ServiceInjection serviceInjection = new ServiceInjection(stormServices);
        pf.addCreationHandler(serviceInjection);

        log.debug("Creating processor-list from element {}", element);
        List<Processor> list = pf.createNestedProcessors(element);

        process = new ProcessorList();
        for (Processor p : list) {
            process.getProcessors().add(p);
        }

        if (element.hasAttribute("output")) {
            String out = element.getAttribute("output");
            if (out.indexOf(",") > 0) {
                outputs = out.split(",");
            } else {
                outputs = new String[]{out};
            }
            Grouping grouping = key != null ? Grouping.GROUPBY : Grouping.SHUFFLE;
            for (String output : outputs) {
                subscriptions.add(new Subscription(output, uuid, output, grouping, key));
            }
        }

        subscriptions.addAll(queueInjection.getSubscriptions());
        log.debug("Found {} subscribers for bolt '{}': {}",
                subscriptions.size(), uuid, subscriptions);
        return process;
    }

    /**
     * @see org.apache.storm.task.IBolt#prepare(Map, TopologyContext, OutputCollector)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        //
        // creating context
        //
        this.componentId = "" + context.getThisComponentId();
        this.taskId = "" + context.getThisTaskId();

        variables.set("copy.id", this.componentId + "-" + String.valueOf(context.getThisTaskIndex()));

        ProcessContext processContext = new TaskContext(taskId, this.ctx);

        log.debug("Preparing ProcessBolt {}", this.uuid);
        this.output = collector;
        log.debug("   output collector: {}", output);

        try {

            process = createProcess();
            process.init(processContext);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create process!");
        }

        log.info("ProcessBolt ready:  {}", this);
    }

    /**
     * @see org.apache.storm.task.IBolt#execute(Tuple)
     */
    @Override
    public void execute(Tuple input) {
        log.trace("Tuple received: {}", input);

        Data item = TupleWrapper.unwrap(input);

        if (item != null) {
            log.trace("Processing item...");
            currentTuple = input;
            item = process.process(item);

            Values emitValue;
            if (key != null && item.containsKey(key)) {
                emitValue = new Values(item.get(key), item);
            } else {
                if (key != null && !item.containsKey(key)) {
                    log.error("Key '" + key + "' not found in the data item. Using random grouping.");
                }
                emitValue = new Values(item);
            }
            if (outputs != null) {
                for (String out : outputs) {
                    log.debug("Emitting result item to {}: {}", out, item);
                    // emit produced tuple to the output while anchoring given tuple
                    output.emit(out, input, emitValue);
                }
            } else {
                log.debug("Emitting item {}", item);
                // emit produced tuple to the output while anchoring given tuple
                output.emit(input, emitValue);
            }

            // acknowledge incoming tuple
            log.debug("Acknowledge in {} anchor {} ", uuid, currentTuple);
            if (acking) {
                output.ack(input);
            }
        } else {
            log.debug("No item to process!");
            // acknowledge incoming tuple as failed
            if (acking) {
                output.fail(input);
            }
        }
    }

    /**
     * @see BaseRichBolt#cleanup()
     */
    @Override
    public void cleanup() {
        super.cleanup();

        try {
            log.debug("Finishing process {}", uuid);
            process.finish();
        } catch (Exception e) {
            log.error("Failed to properly shutdown process {}: {}", this.uuid, e.getMessage());
        }
    }

    public Object readResolve() {
        log.debug("readResolve() -> {}", this);
        return this;
    }

    public String toString() {
        return super.toString() + "(componentId: " + this.componentId + ", taskId:" + this.taskId + ")";
    }
}