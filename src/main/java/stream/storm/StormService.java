package stream.storm;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.util.Map;

import stream.StreamTopology;
import stream.runtime.ApplicationContext;
import stream.runtime.setup.factory.ObjectFactory;
import stream.service.Service;
import stream.util.Variables;

/**
 * StormService is used as a wrapper for a service class. While building up storm topology, all
 * services are wrapped into StormServices, initialized after deserialization by storm and then set
 * by ProcessorCreationHandler to be used by a right processor.
 *
 * @author alexey
 */
public class StormService extends AbstractBolt {

    static Logger log = LoggerFactory.getLogger(StormService.class);

    /**
     * Variables with environment information
     */
    private final Variables variables;

    /**
     * Document element containing information about service.
     */
    private final Element element;

    /**
     * Service to be used
     */
    private Service service;

    public StormService(ApplicationContext ctx, String xmlConfig, String uuid,
                        StreamTopology streamTopology, Element element) {
        super(ctx, xmlConfig, uuid);
        if (!element.hasAttribute("class") || !element.hasAttribute("id")) {
            log.error("Class or ID attribute are not given for a service.");
            throw new ExceptionInInitializerError("Class or ID attribute are not given for a service.");
        }
        this.variables = streamTopology.variables;
        this.element = element;
        log.info("Creating new service implementation from class {}", element.getAttribute("class"));
    }

    /**
     * Retrieve service object.
     */
    public Service getService() {
        return service;
    }

    public String getServiceName() {
        return element.getAttribute("id");
    }

    @Override
    public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
        // only call prepareService() if it has not been done before it, but readResolve()
        // should definitely has been called
        if (service == null) {
            prepareService();
        }
    }

    /**
     * Called after deserialization to prepare (initialize) service.
     */
    public Object readResolve() throws Exception {
        prepareService();
        return this;
    }

    /**
     * Initialize service using element's attributes (class, id, etc.)
     */
    private void prepareService() {
        log.info("Initializing service implementation from class {}", element.getAttribute("class"));
        ObjectFactory obf = ObjectFactory.newInstance();
        obf.addVariables(variables);
        Map<String, String> params = obf.getAttributes(element);
        try {
            service = (Service) obf.create(params.get("class"), params,
                    ObjectFactory.createConfigDocument(element), variables);
            service.reset();
        } catch (Exception e) {
            log.error("Failed to create and register service '{}': {}",
                    params.get("id"), e.getMessage());
        }
    }

    @Override
    public void execute(Tuple tuple) {
    }
}
