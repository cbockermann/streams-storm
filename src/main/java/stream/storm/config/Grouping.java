package stream.storm.config;

/**
 * Created by alexey on 11/10/2016.
 */
public enum Grouping {
    GROUPBY, SHUFFLE;


    @Override
    public String toString() {
        switch (this) {
            case GROUPBY: return "groupby";
            case SHUFFLE: return "shuffle";
            default: throw new IllegalArgumentException(super.toString());
        }
    }
}
