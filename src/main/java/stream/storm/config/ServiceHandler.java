package stream.storm.config;

import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.util.Map;

import stream.StreamTopology;
import stream.runtime.DefaultApplicationContext;
import stream.runtime.setup.factory.ObjectFactory;
import stream.storm.StormService;

/**
 * Configuration handler for streams' services. Method handle(...) creates FlinkService to wrap the
 * underlying service.
 *
 * @author alexey
 */
public class ServiceHandler implements ConfigHandler {

    static Logger log = LoggerFactory.getLogger(ServiceHandler.class);
    public StormService service;
    private DefaultApplicationContext context;
    private String xml;

    public ServiceHandler(DefaultApplicationContext context, String xml) {
        this.context = context;
        this.xml = xml;
    }

    @Override
    public void handle(Element element, StreamTopology st, TopologyBuilder builder) throws Exception {
        log.debug("handling element {}...", element);
        Map<String, String> params = ObjectFactory.newInstance().getAttributes(element);

        String className = params.get("class");
        if (className == null || "".equals(className.trim())) {
            throw new Exception("No class name provided in 'class' attribute if Service element!");
        }

        String id = params.get("id");
        if (id == null || "".equals(id.trim())) {
            throw new Exception("No valid 'id' attribute provided for Service element!");
        }

        service = new StormService(context, xml, id, st, element);
    }

    @Override
    public boolean handles(Element el) {
        return "service".equals(el.getNodeName().toLowerCase());
    }
}
