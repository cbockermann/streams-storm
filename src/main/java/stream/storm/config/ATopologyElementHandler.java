/*
 *  streams library
 *
 *  Copyright (C) 2011-2014 by Christian Bockermann, Hendrik Blom
 * 
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.storm.config;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import stream.runtime.DefaultApplicationContext;
import stream.runtime.setup.factory.ObjectFactory;

/**
 * @author chris
 * 
 */
public abstract class ATopologyElementHandler implements ConfigHandler {

	protected final ObjectFactory objectFactory;
	protected final DefaultApplicationContext appContext;

	public ATopologyElementHandler(DefaultApplicationContext ctx, ObjectFactory of) {
		this.objectFactory = of;
		this.appContext = ctx;
	}

	protected List<String> getInputNames(Element el) {
		List<String> inputs = new ArrayList<>();
		String input = el.getAttribute("input");
		if (input == null)
			return inputs;

		if (!input.contains(",")) {
			inputs.add(input.trim());
			return inputs;
		}

		for (String in : input.split(",")) {
			if (in != null && !in.trim().isEmpty()) {
				inputs.add(in.trim());
			}
		}
		return inputs;
	}

	protected String getAttribute(Element el, String attribute, String defaultValue) {
		if (el != null && el.hasAttribute(attribute)) {
			String val = el.getAttribute(attribute);
			if (val != null) {
				return val;
			}
		}

		return defaultValue;
	}
}
