/*
 *  streams library
 *
 *  Copyright (C) 2011-2014 by Christian Bockermann, Hendrik Blom
 * 
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.storm.config;

import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import java.util.List;

import stream.Constants;
import stream.StreamTopology;
import stream.Subscription;
import stream.runtime.DefaultApplicationContext;
import stream.runtime.setup.factory.ObjectFactory;
import stream.storm.ProcessBolt;
import stream.util.Variables;

/**
 * @author chris
 */
public class ProcessHandler extends ATopologyElementHandler {

    static Logger log = LoggerFactory.getLogger(ProcessHandler.class);

    final String xml; // the xml string (config)

    /**
     * @param of
     */
    public ProcessHandler(DefaultApplicationContext ctx, ObjectFactory of, String xml) {
        super(ctx, of);
        this.xml = xml;
    }

    /**
     * @see stream.storm.config.ConfigHandler#handles(org.w3c.dom.Element)
     */
    @Override
    public boolean handles(Element el) {
        String name = el.getNodeName();
        return name.equalsIgnoreCase("process");
    }

    /**
     * @see stream.storm.config.ConfigHandler#handle(Element, StreamTopology, TopologyBuilder)
     */
    @Override
    public void handle(Element el, StreamTopology st, TopologyBuilder builder) throws Exception {

        if (el.getNodeName().equalsIgnoreCase("process")) {
            String id = el.getAttribute(Constants.ID);
            if (id == null || id.trim().isEmpty()) {
                log.error("No 'id' attribute defined in process element (class: '{}')", el.getAttribute("class"));
                throw new Exception("Missing 'id' attribute for process element!");
            }

            log.info("  > Creating process-bolt with id '{}'", id);

            int numTasks = Integer.parseInt(getAttribute(el, "tasks", "0"));
            Integer workers = Utils.getLevelOfParallelism(el);

            Variables local = new Variables(st.getVariables());
            local.set("copy.id", "0");
            //st.variables.clear();
            st.variables.addVariables(local);
            // id = local.expand(id);

            final ProcessBolt bolt = new ProcessBolt(appContext, xml, id, st);
            log.info("  >   Registering bolt (process) '{}' with instance {}", id, bolt);

            final String groupByKey = createGrouping(el);

            final List<String> inputs = getInputNames(el);

            final BoltDeclarer cur = builder.setBolt(id, bolt, workers);

            log.info("  >   Creating {} tasks for bolt '{}'", workers, id);
            if (numTasks > 0) {
                cur.setNumTasks(numTasks);
            }

            if (!inputs.isEmpty()) {
                for (String in : inputs) {
                    if (!in.isEmpty()) {
                        //
                        // if 'in' is reference to a process/bolt
                        //
                        Grouping groupType = groupByKey != null ? Grouping.GROUPBY : Grouping.SHUFFLE;
                        Subscription subscription;
                        if (groupType == Grouping.GROUPBY) {
                            subscription = new Subscription(id, in, groupType, groupByKey);
                        } else {
                            subscription = new Subscription(id, in, groupType);
                        }
                        log.info("  >   Connecting bolt '{}' to '{}' with grouping " + groupType.toString(), id, in);
                        st.addSubscription(subscription);
                    }
                }
            } else {
                log.warn("No input defined for process '{}'!", id);
            }
            st.addBolt(id, cur);

            for (Subscription subscription : bolt.getSubscriptions()) {
                log.info("Adding subscription:  {}", subscription);
                st.addSubscription(subscription);
            }
        }
    }

    public String createGrouping(Element e) {
        String groupBy = e.getAttribute("groupBy");
        if (groupBy == null || groupBy.trim().isEmpty()) {
            return null;
        } else {
            return groupBy;
        }
    }
}
