package stream.storm.config;

import org.w3c.dom.Element;

import stream.Constants;
import stream.util.Variables;

/**
 * @author alexey
 */
public class Utils {

    /**
     * Extract level of parallelism from given element.
     *
     * @param element element of XML document containing bolt or spout
     * @return level of parallelism or 1 if no attribute specified
     */
    public static Integer getLevelOfParallelism(Element element) {
        String copies = element.getAttribute(Constants.PARALLELISM);
        Integer workers = 1;
        if (copies != null && !copies.isEmpty()) {
            try {
                workers = Integer.parseInt(copies);
            } catch (Exception e) {
                throw new RuntimeException("Invalid number of copies '" + copies + "' specified!");
            }
        }
        return workers;
    }
}
