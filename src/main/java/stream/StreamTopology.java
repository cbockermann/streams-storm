/*
 *  streams library
 *
 *  Copyright (C) 2011-2014 by Christian Bockermann, Hendrik Blom
 *
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can
 *  redistribute it and/or modify it under the terms of the
 *  GNU Affero General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream;

import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.SpoutDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import stream.runtime.DefaultApplicationContext;
import stream.runtime.DependencyInjection;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.handler.PropertiesHandler;
import stream.storm.StormService;
import stream.storm.config.BoltHandler;
import stream.storm.config.ConfigHandler;
import stream.storm.config.Grouping;
import stream.storm.config.ProcessHandler;
import stream.storm.config.QueueHandler;
import stream.storm.config.ServiceHandler;
import stream.storm.config.SpoutHandler;
import stream.storm.config.StreamHandler;
import stream.util.Variables;
import stream.util.XMLUtils;

/**
 * StreamTopology parses the job definition from the XML file and constructs a Storm topology using
 * TopologyBuilder.
 *
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 */
public class StreamTopology {

    static Logger log = LoggerFactory.getLogger(StreamTopology.class);

    public final static String UUID_ATTRIBUTE = "stream.storm.uuid";

    static String xml;

    static DefaultApplicationContext context;

    public final TopologyBuilder builder;

    /**
     * Set of all spouts
     */
    public final Map<String, SpoutDeclarer> spouts = new LinkedHashMap<>();

    /**
     * Set of all bolts
     */
    public final Map<String, BoltDeclarer> bolts = new LinkedHashMap<>();

    public final Variables variables = new Variables();

    final Set<Subscription> subscriptions = new LinkedHashSet<>();

    /**
     * List of services
     */
    public List<StormService> stormServices = new ArrayList<>(0);

    /**
     * Option to enable acknowledging of messages
     */
    public boolean acking = false;

    private StreamTopology(TopologyBuilder builder) {
        this.builder = builder;
    }

    public TopologyBuilder getTopologyBuilder() {
        return builder;
    }

    public Variables getVariables() {
        return variables;
    }

    public void addSubscription(Subscription sub) {
        subscriptions.add(sub);
    }

    /**
     * Creates a new instance of a StreamTopology based on the given document. This also creates a
     * standard TopologyBuilder to build the associated Storm Topology.
     *
     * @param doc The DOM document that defines the topology.
     */
    public static StreamTopology create(Document doc) throws Exception {
        return build(doc, new TopologyBuilder());
    }

    /**
     * Creates a new instance of a StreamTopology based on the given document and using the
     * specified TopologyBuilder.
     */
    public static StreamTopology build(Document doc, TopologyBuilder builder) throws Exception {

        final StreamTopology st = new StreamTopology(builder);

        // try to find application or container tags
        NodeList nodeList = doc.getElementsByTagName("application");
        if (nodeList.getLength() < 1) {
            nodeList = doc.getElementsByTagName("container");
        }

        // do there exist more than one application or container tags?
        if (nodeList.getLength() > 1 || nodeList.getLength() == 0) {
            log.info("\n\n--------\nstreams needs exactly one container or application tag." +
                    "\n--------");
            System.exit(-1);
        }

        doc = XMLUtils.addUUIDAttributes(doc, UUID_ATTRIBUTE);
        String id = getAppId(nodeList);
        st.getVariables().put(Constants.APPID, id);

        try{
            String ack = nodeList.item(0).getAttributes().getNamedItem("ack").getNodeValue();
            if (ack.toLowerCase().equals("true")) {
                st.acking = true;
                log.info("Enable acking for topology.");
            }
        } catch (NullPointerException exc) {
            log.error("No acking attribute was found in container/application tag.");
        }

        context = new DefaultApplicationContext(id, st.getVariables());
        xml = XMLUtils.toString(doc);

        // a map of pre-defined inputs, i.e. input-names => uuids
        // to catch the case when processes read from queues that have
        // not been explicitly defined (i.e. 'linking bolts')
        //
        // Map<String, String> streams = new LinkedHashMap<String, String>();

        st.initProperties(doc);

        stream.runtime.StreamRuntime.loadUserProperties();

        replaceVariables(st.variables, doc.getDocumentElement());

        st.initStormServices(doc, builder);

        st.handleConfigurations(doc, builder);

        st.resolveSubscriptions();

        return st;
    }

    /**
     * Replace the '${xxx}' expressions with properties kept in variables.
     *  @param variables map of property values set through system or xml
     * @param node current node
     */
    public static void replaceVariables(Variables variables, Node node) {
        NamedNodeMap attributes = node.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
            Node item = attributes.item(i);
            String nodeValue = item.getNodeValue();
            if (nodeValue.contains("${") && !(nodeValue.contains("copy.id"))) {
                nodeValue = variables.expand(nodeValue);
                item.setNodeValue(nodeValue);
            }
        }

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                replaceVariables(variables, currentNode);
            }
        }
    }

    /**
     * Initialize property tags and add them to variables.
     *
     * @param doc XML document
     */
    private void initProperties(Document doc) {
        try {
            PropertiesHandler handler = new PropertiesHandler();
            handler.handle(null, doc, getVariables(), new DependencyInjection());

            if (log.isDebugEnabled()) {
                log.debug("########################################################################");
                log.debug("Found properties: {}", getVariables());
                for (String key : getVariables().keySet()) {
                    log.debug("   '{}' = '{}'", key, getVariables().get(key));
                }
                log.debug("########################################################################");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Find all services and wrap them in StormServices.
     *
     * @param doc     XML document
     * @param builder topology builder
     */
    private void initStormServices(Document doc, TopologyBuilder builder) throws Exception {
        NodeList servicesList = doc.getDocumentElement().getElementsByTagName("service");
        ServiceHandler serviceHandler = new ServiceHandler(context, xml);
        for (int iq = 0; iq < servicesList.getLength(); iq++) {
            Element element = (Element) servicesList.item(iq);
            if (serviceHandler.handles(element)) {
                serviceHandler.handle(element, this, builder);
                StormService stormService = serviceHandler.service;
                stormServices.add(stormService);
                //TODO: do we need to generate BOLTS for services? or use some other structure?
            }
        }
    }

    /**
     * Iterate through different handlers for spout, queue, stream, bolt and process and handle the
     * element tags contained in the XML configuration file.
     *
     * @param doc     XML document
     * @param builder topology builder
     */
    private void handleConfigurations(Document doc, TopologyBuilder builder) throws Exception {
        ObjectFactory of = ObjectFactory.newInstance();
        of.addVariables(getVariables());
        List<ConfigHandler> handlers = new ArrayList<>();
        handlers.add(new SpoutHandler(context, of));
        handlers.add(new QueueHandler(context, of, xml));
        handlers.add(new StreamHandler(context, of, xml));
        handlers.add(new BoltHandler(context, of));
        handlers.add(new ProcessHandler(context, of, xml));

        NodeList list = doc.getDocumentElement().getChildNodes();
        int length = list.getLength();

        for (ConfigHandler handler : handlers) {

            for (int i = 0; i < length; i++) {
                Node node = list.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element el = (Element) node;

                    if (handler.handles(el)) {
                        log.info("--------------------------------------------------------------------------------");
                        log.info("Handling element '{}'", node.getNodeName());
                        handler.handle(el, this, builder);
                        log.info("--------------------------------------------------------------------------------");
                    }
                }
            }
        }
    }

    /**
     * Go through all subscriptions and set sources for subscribers.
     */
    private void resolveSubscriptions() throws Exception {
        Iterator<Subscription> it = subscriptions.iterator();
        log.info("--------------------------------------------------------------------------------");
        while (it.hasNext()) {
            Subscription s = it.next();
            log.info("   {}", s);
        }
        log.info("--------------------------------------------------------------------------------");
        it = subscriptions.iterator();

        while (it.hasNext()) {
            Subscription subscription = it.next();
            log.info("Resolving subscription {}", subscription);

            BoltDeclarer subscriber = bolts.get(subscription.subscriber());
            if (subscriber != null) {
                String source = subscription.source();
                String stream = subscription.stream();
                if (stream.equals("default")) {
                    //TODO: is it senseless?
                    initGroupedSubscription(subscription, subscriber, source, "default");
                } else {
                    initGroupedSubscription(subscription, subscriber, source, stream);
                }
                it.remove();
            } else {
                log.error("No subscriber found for id '{}'", subscription.subscriber());
            }
        }

        if (!subscriptions.isEmpty()) {
            log.info("Unresolved subscriptions: {}", subscriptions);
            throw new Exception("Found " + subscriptions.size()
                    + " unresolved subscription references!");
        }
    }

    private void initGroupedSubscription(Subscription subscription, BoltDeclarer subscriber, String source, String stream) {
        if (subscription.grouping() == Grouping.GROUPBY) {
            log.info("connecting '{}' to groupby-{} '{}:" + stream + "'",
                    subscription.subscriber(), subscription.groupByKey(), source);
            subscriber.fieldsGrouping(source, stream, new Fields(subscription.groupByKey()));
        } else {
            log.info("connecting '{}' to none-group '{}:" + stream + "'",
                    subscription.subscriber(), source);
            subscriber.shuffleGrouping(source, stream);
        }
        //TODO add other grouping possibilities
    }

    /**
     * Search for application id in application and container tags. Otherwise produce random UUID.
     *
     * @param nodeList list of
     * @return application id extracted from the ID attribute or random UUID if no ID attribute
     * present
     */
    private static String getAppId(NodeList nodeList) {
        String appId = "application:" + UUID.randomUUID().toString();
        try {
            appId = nodeList.item(0).getAttributes().getNamedItem("id").getNodeValue();
        } catch(NullPointerException exc) {
            log.error("No 'id' attribute has been found. Use {} instead.", appId);
        }
        return appId;
    }

    public void addBolt(String id, BoltDeclarer bolt) {
        bolts.put(id, bolt);
    }

    public void addSpout(String id, SpoutDeclarer spout) {
        spouts.put(id, spout);
    }

    /**
     * This method creates a new instance of type StormTopology based on the topology that has been
     * created from the DOM document.
     */
    public StormTopology createTopology() {
        return builder.createTopology();
    }

    public static void main(String[] args) throws Exception {

        if (args.length != 1) {
            System.err.println("Missing XML definition (base64 encoded)!");
            return;
        }

        Document doc = DocumentEncoder.decodeDocument(args[0]);
        Config conf = new Config();
        conf.setNumWorkers(20);

        StreamTopology streamTop = build(doc, new TopologyBuilder());
        StormTopology topology = streamTop.getTopologyBuilder().createTopology();

        StormSubmitter.submitTopology("test", conf, topology);
    }
}
