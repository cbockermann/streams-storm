package stream;

/**
 * @author alexey
 */
public class Constants {
    public static final String ID = "id";
    public static final String NUM_WORKERS = "stream.storm.workers";
    public static final String APPID = "application.id";
    public static final String PARALLELISM = "copies";
    public static final int MAX_SPOUT_PENDING = 3000;
}
