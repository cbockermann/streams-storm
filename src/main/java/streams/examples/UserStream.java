/**
 * 
 */
package streams.examples;

import java.util.Random;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;

/**
 * @author chris
 *
 */
public class UserStream extends AbstractStream {

	long time = System.currentTimeMillis();
	final Random rnd = new Random();
	String[] users;

	public UserStream() {
		users = "A,B,C,D,E,F,G".split(",");
		time = System.currentTimeMillis();
	}

	/**
	 * @see stream.io.AbstractStream#readNext()
	 */
	@Override
	public synchronized Data readNext() throws Exception {

		// Thread.sleep(10);

		time = System.currentTimeMillis();
		Data item = DataFactory.create();
		item.put("@timestamp", time);

		String user = users[rnd.nextInt(users.length)];
		item.put("user", user);

		return item;
	}
}
