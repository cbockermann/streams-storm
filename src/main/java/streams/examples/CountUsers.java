/**
 * 
 */
package streams.examples;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.util.MultiSet;

/**
 * @author chris
 *
 */
public class CountUsers extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(CountUsers.class);

	final MultiSet<String> users = new MultiSet<String>();
	String key = "user";

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		Serializable value = input.get(key);
		if (value != null) {
			users.add(value.toString());
		}
		return input;
	}

	/**
	 * @see stream.AbstractProcessor#finish()
	 */
	@Override
	public void finish() throws Exception {
		super.finish();

		String id = this.context.getId();

		for (String user : users) {
			log.info(id + "   '{}' => {}", user, users.count(user));
		}
	}
}
