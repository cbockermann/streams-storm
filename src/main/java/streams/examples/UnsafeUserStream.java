/**
 * 
 */
package streams.examples;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.data.DataFactory;
import stream.data.SequenceID;
import stream.io.AbstractStream;

/**
 * @author chris
 *
 */
public class UnsafeUserStream extends AbstractStream {

	static Logger log = LoggerFactory.getLogger(UnsafeUserStream.class);

	long time = System.currentTimeMillis();
	final Random rnd = new Random();
	String[] users;

	public UnsafeUserStream() {
		users = "A,B,C,D,E,F,G".split(",");
		time = System.currentTimeMillis();
	}

	/**
	 * @see stream.io.Stream#read()
	 */
	public Data read() throws Exception {

		if (closed || (limit > 0 && count > limit))
			return null;

		Data datum = readNext();
		if (datum == null) {
			log.debug("End-of-stream reached!");
			return null;
		}

		if (this.id != null)
			datum.put("@stream", this.id);

		if (this.sequenceKey != null) {
			SequenceID next = this.seqId.getAndIncrement();
			datum.put(sequenceKey, next);
		}
		if (prefix != null && !prefix.trim().isEmpty()) {
			Data prefixed = DataFactory.create();
			for (String key : datum.keySet()) {
				prefixed.put(prefix + ":" + key, datum.get(key));
			}
			datum = prefixed;
		}

		count++;
		return datum;
	}

	/**
	 * @see stream.io.AbstractStream#readNext()
	 */
	@Override
	public Data readNext() throws Exception {

		// Thread.sleep(10);

		time = System.currentTimeMillis();
		Data item = DataFactory.create();
		item.put("@timestamp", time);

		String user = users[rnd.nextInt(users.length)];
		item.put("user", user);

		return item;
	}
}
