/**
 * 
 */
package streams.storm;

import java.util.Collection;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Values;

import stream.Data;
import stream.io.Sink;

/**
 * This class implements a simple sink, that may be injected into a processor.
 * Any items, which are written to that sink, will be send out via the attached
 * output collector.
 * 
 * @author Christian Bockermann
 *
 */
public class SinkOutputAdapter implements Sink {

    String id;
    final OutputCollector output;

    public SinkOutputAdapter(String id, OutputCollector output) {
        this.id = id;
        this.output = output;
    }

    /**
     * @see stream.io.Sink#getId()
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * @see stream.io.Sink#write(stream.Data)
     */
    @Override
    public boolean write(Data item) throws Exception {
        if (item == null)
            return false;
        output.emit(id, new Values(item));
        return true;
    }

    @Override
    public void close() throws Exception {
    }

    @Override
    public boolean write(Collection<Data> data) throws Exception {

        for (Data item : data) {
            output.emit(id, new Values(item));
        }

        return true;
    }

    /**
     * @see stream.io.Sink#setId(java.lang.String)
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @see stream.io.Sink#init()
     */
    @Override
    public void init() throws Exception {
    }
}
