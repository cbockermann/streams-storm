/**
 * 
 */
package streams.storm;

import stream.AbstractContext;
import stream.ProcessContext;
import stream.runtime.DefaultApplicationContext;
import stream.storm.BoltContext;
import stream.util.Variables;

/**
 * This class implements the context of a <i>task</i>. A task is an instance of
 * a process, that is being executed. It has a parent context, which is the
 * application context (container/topology).
 * 
 * @author Christian Bockermann
 *
 */
public class TaskContext extends AbstractContext implements ProcessContext {

	public TaskContext(String taskId, BoltContext app) {
		super(taskId, app);
	}

	/**
	 * @see stream.ProcessContext#set(java.lang.String, java.lang.Object)
	 */
	@Override
	public void set(String key, Object o) {
		String k = key;
		if (key.startsWith(scope() + ".")) {
			k = key.substring(scope().length() + 1);
		}
		if (o == null) {
			values.remove(k);
		} else {
			values.put(k, o);
		}
	}

	/**
	 * @see stream.ProcessContext#clear()
	 */
	@Override
	public void clear() {
		values.clear();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see stream.ProcessContext#get(java.lang.String)
	 */
	@Override
	public Object get(String key) {
		return resolve(key);
	}

	/**
	 * @see stream.AbstractContext#scope()
	 */
	@Override
	public String scope() {
		return "process";
	}

	public static void main(String[] args) {
		Variables vars = new Variables();
		vars.set("testA", "B");
		DefaultApplicationContext ac = new DefaultApplicationContext("test-topology", vars);

		BoltContext bc = new BoltContext(ac, "0");
		TaskContext tc = new TaskContext("task:1", bc);

		System.out.println("ApplicationContext prefix: " + ac.prefix());
		System.out.println("TaskContext prefix: " + tc.scope());

		System.out.println("application.testA ~> " + tc.get("application.testA"));
		System.out.println("container.testA ~> " + tc.get("container.testA"));

		System.out.println("tc.path = " + tc.path());
	}
}
