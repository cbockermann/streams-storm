/**
 * 
 */
package streams.storm.grouping;

import java.util.ArrayList;
import java.util.List;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 *
 */
public class RandomGrouping implements CustomStreamGrouping {

    static Logger log = LoggerFactory.getLogger(RandomGrouping.class);

    private static final long serialVersionUID = 8733667425487674053L;

    int[] taskMap;
    int last = 0;

    /**
     * @see org.apache.storm.grouping.CustomStreamGrouping#prepare(WorkerTopologyContext, GlobalStreamId, List)
     */
    @Override
    public void prepare(WorkerTopologyContext context, GlobalStreamId stream, List<Integer> tasks) {
        taskMap = new int[tasks.size()];
        for (int i = 0; i < tasks.size(); i++) {
            taskMap[i] = tasks.get(i);
        }
    }

    /**
     * @see org.apache.storm.grouping.CustomStreamGrouping#chooseTasks(int, List)
     */
    @Override
    public List<Integer> chooseTasks(int arg0, List<Object> arg1) {
        List<Integer> map = new ArrayList<Integer>();
        map.add(taskMap[last % taskMap.length]);
        last = (last + 1) % taskMap.length;
        return map;
    }
}
