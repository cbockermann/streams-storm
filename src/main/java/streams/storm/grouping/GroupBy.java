/**
 * 
 */
package streams.storm.grouping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.grouping.CustomStreamGrouping;
import org.apache.storm.task.WorkerTopologyContext;
import org.apache.storm.tuple.Fields;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import streams.storm.TupleWrapper;

/**
 * @author chris
 *
 */
public class GroupBy implements CustomStreamGrouping {

    private static final long serialVersionUID = 4546407542738572156L;

    static Logger log = LoggerFactory.getLogger(GroupBy.class);

    String key;
    int[] tasks;
    String[] fields;

    AtomicInteger next = new AtomicInteger(0);
    int itemIndex = -1;

    public GroupBy() {
        this(null);
    }

    public GroupBy(String key) {
        this.key = key;
        log.debug("Creating groupBy('{}')", this.key);
    }

    /**
     * @see org.apache.storm.grouping.CustomStreamGrouping#prepare(WorkerTopologyContext, GlobalStreamId, List)
     */
    @Override
    public void prepare(WorkerTopologyContext context, GlobalStreamId streamId, List<Integer> targetTasks) {
        log.debug("Preparing grouping by '{}'", key);

        log.debug("   globalStreamId = {}", streamId);

        final Fields fields = context.getComponentOutputFields(streamId);
        log.debug("   stream '{}’ has the following fields:", streamId);
        int f = 0;
        this.fields = new String[fields.size()];
        for (String field : fields) {
            log.debug("     field[{}] = {}", f, field);
            this.fields[f] = field;
            if (field.equals(TupleWrapper.DATA_KEY)) {
                itemIndex = f;
                log.debug("piggy-pack index is {}", itemIndex);
            }
        }

        log.debug("   maintaining a mapping to {} tasks", targetTasks.size());
        tasks = new int[targetTasks.size()];
        for (int i = 0; i < targetTasks.size(); i++) {
            tasks[i] = targetTasks.get(i);
            log.debug("     - task[{}] = {}", i, tasks[i]);
        }
    }

    /**
     * @see org.apache.storm.grouping.CustomStreamGrouping#chooseTasks(int, List)
     */
    @Override
    public List<Integer> chooseTasks(int arg0, List<Object> values) {
        final List<Integer> tasks = new ArrayList<Integer>();
        log.debug("grouping by key '{}', data item is: {}", key, values.get(itemIndex));

        final Data item = (Data) values.get(itemIndex);
        final Serializable value = item.get(key);
        log.debug("   grouping-value is: '{}'", value);

        if (key == null || value == null) {
            log.warn("No way to map item to a group: key={}, value={}", key, value);
        } else {
            int idx = item.get(key).hashCode() % this.tasks.length;
            tasks.add(this.tasks[idx]);
        }

        log.debug("   {} -> {}", item, tasks);
        return tasks;
    }

    public String getGroupByKey(){
        return key;
    }
}