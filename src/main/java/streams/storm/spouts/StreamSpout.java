/*
 *  streams library
 *
 *  Copyright (C) 2011-2014 by Christian Bockermann, Hendrik Blom
 * 
 *  streams is a library, API and runtime environment for processing high
 *  volume data streams. It is composed of three submodules "stream-api",
 *  "stream-core" and "stream-runtime".
 *
 *  The streams library (and its submodules) is free software: you can 
 *  redistribute it and/or modify it under the terms of the 
 *  GNU Affero General Public License as published by the Free Software 
 *  Foundation, either version 3 of the License, or (at your option) any 
 *  later version.
 *
 *  The stream.ai library (and its submodules) is distributed in the hope
 *  that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.storm.spouts;

import org.apache.storm.spout.ISpout;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.List;
import java.util.Map;

import stream.Data;
import stream.io.Stream;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.StreamFactory;
import stream.storm.config.StreamHandler.StreamFinder;
import stream.util.Variables;
import stream.util.XMLUtils;
import streams.storm.TupleWrapper;

/**
 * @author Christian Bockermann, Alexey Egorov
 */
public class StreamSpout extends BaseRichSpout {

    static Logger log = LoggerFactory.getLogger(StreamSpout.class);

    /**
     * The unique class ID
     */
    private static final long serialVersionUID = -786482575770711600L;

    /**
     * Sleeptime for the case reading next item produced NULL (wait for reading non-null value).
     */
    public static final int SLEEPTIME = 500;

    /**
     * If reading next item fails, then stream is supposed to have ended and outputting error
     * message about NULL item will be performed only once.
     */
    private boolean streamEnded;

    transient Stream stream;

    /**
     * Output collector for emitting, acknowledging and anchoring tuples.
     */
    protected SpoutOutputCollector output;

    /**
     * System and custom process variables
     */
    protected final Variables parameters;
    protected final String xml;
    protected final String id;

    /**
     * Id of an emitted message (tuple) to be acknowledged.
     */
    private int messageId = 0;

    /**
     * Counter for acknowledged/failed messages (+1 and -1 respectively).
     */
    private int messageCounter = 0;

    /**
     * Id of the component inside the topology
     */
    String componentId;

    /**
     * Id of the task (e.g. in case of multiple instances of a stream)
     */
    String taskId;

    /**
     * Key used for grouping the data stream
     */
    String key = null;

    /**
     * True if all the sent messages (number of messages) has been processed by the topology.
     */
    boolean lastItemProcessed = false;

    /**
     * Option to enable acknowledging of messages
     */
    boolean acking = false;

    public StreamSpout(String xml, String id, Map<String, String> params, boolean acking) throws Exception {
        log.info("Creating spout for stream (params: {})", params);
        this.xml = xml;
        this.id = id;
        this.parameters = new Variables(params);
        this.acking = acking;
        stream = createStream();
        streamEnded = false;
    }

    /**
     * Create new stream object using stream factory.
     */
    protected Stream createStream() throws Exception {
        Document doc = XMLUtils.parseDocument(xml);
        List<Element> els = XMLUtils.findElements(doc, new StreamFinder(id));

        if (els.size() != 1) {
            throw new RuntimeException("Failed to locate 'stream' element for id '" + id + "'!");
        }

        Element el = els.get(0);
        if (el.hasAttribute("key")) {
            key = el.getAttribute("key");
        }
        ObjectFactory objectFactory = ObjectFactory.newInstance();
        Stream stream = StreamFactory.createStream(objectFactory, el, parameters);
        return stream;
    }

    /**
     * @see org.apache.storm.spout.ISpout#open(Map, TopologyContext, SpoutOutputCollector)
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {

        this.componentId = "" + context.getThisComponentId();
        this.taskId = "" + context.getThisTaskId();

        log.info("Opening StreamSpout {} for stream '{}'", this, id);
        this.output = collector;
        try {
            if (stream == null) {
                stream = createStream();
            }
            stream.init();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to open stream: " + e.getMessage());
        }

        log.info("StreamSpout open:   {}", this);
    }

    @Override
    public void fail(Object msgId) {
        super.fail(msgId);
        messageCounter--;
        log.info("Message {} failed.", msgId);
    }

    @Override
    public void ack(Object msgId) {
        super.ack(msgId);
        messageCounter++;
        if (messageCounter == messageId - 1) {
            lastItemProcessed = true;
        }
        log.debug("Processed {} out of {} sent messages.", messageCounter, messageId);
        log.debug("Message {} acknowledged.", msgId);
    }

    /**
     * @see ISpout#nextTuple()
     */
    @Override
    public void nextTuple() {
        log.trace("nextTuple() called");
        try {
            Data item = stream.read();
            streamEnded = false;
            log.debug("read item: {}", item);
            if (item == null) {
                if (lastItemProcessed) {
                    log.info("Stop the application as all items have been processed.");
                    System.exit(0);
                } else {
                    log.debug("Sleep for {} after empty data item arrived.", SLEEPTIME);
                    sleep(SLEEPTIME);
                }
            } else {
                log.debug("Emitting item as tuple...");
                item.put(Stream.SOURCE_KEY, componentId + "-task:" + taskId);

                Values emitValue;
                if (key != null && item.containsKey(key)) {
                    emitValue = new Values(item.get(key), item);
                } else {
                    emitValue = new Values(item);
                }

                lastItemProcessed = false;

                //TODO: make stream spout stateful (save the last id and just count up?)
                //TODO: for parallelized sources such message id wouldn't work
                // emit tuple with a unique identifier for acknowledgement
                if (acking) {
                    output.emit(emitValue, messageId++);
                } else {
                    output.emit(emitValue);
                }
            }
        } catch (Exception e) {
            if (!streamEnded) {
                streamEnded = true;
                log.error("Failed to read next item: {}", e.getMessage());
                if (log.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception e) {
            log.error("Error while sleep in StreamSpout with parameters: {}", parameters);
        }
    }

    /**
     * @see org.apache.storm.topology.IComponent#declareOutputFields(OutputFieldsDeclarer)
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        Fields fields;
        if (key != null) {
            log.debug("Declaring output-fields '{}' and '{}'", key, TupleWrapper.DATA_KEY);
            fields = new Fields(key, TupleWrapper.DATA_KEY);

        } else {
            log.debug("Declaring output-field ''", TupleWrapper.DATA_KEY);
            fields = new Fields(TupleWrapper.DATA_KEY);
        }
        declarer.declare(fields);
    }

    public Object readResolve() {
        log.debug("readResolve() -> {}", this);
        return this;
    }

    public String toString() {
        return super.toString() + "(componentId: " + this.componentId + ", taskId:" + this.taskId + ")";
    }
}