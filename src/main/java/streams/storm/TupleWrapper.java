/**
 * 
 */
package streams.storm;

import java.io.Serializable;

import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import stream.Data;
import stream.data.DataFactory;

/**
 * @author chris
 * 
 */
public final class TupleWrapper {

    public final static String DATA_KEY = "stream.Data";

    public static Data unwrap(Tuple tuple) {

        try {
            return (Data) tuple.getValueByField(DATA_KEY);
        } catch (Exception e) {
            Data item = DataFactory.create();
            Fields fields = tuple.getFields();
            for (int i = 0; i < fields.size(); i++) {
                String key = fields.get(i);
                Object value = tuple.getValue(i);
                if (value instanceof Serializable) {
                    item.put(key, (Serializable) value);
                }
            }

            return item;
        }
    }
}
